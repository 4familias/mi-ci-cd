#reactapp-build

# Set the base image to node:12-alpine
FROM node:12-alpine as build

# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . /app/

# Prepare the container for building React
RUN npm install
RUN npm install react-scripts@3.0.1 -g
# We want the production version
RUN npm run build

# Prepare nginx
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]


#FROM node:current-alpine 

# WORKDIR /app

# EXPOSE 3000 

# ENV PATH /app/node_modules/.bin:$PATH

# COPY package.json package-lock.json ./

# RUN npm install --silent && npm install react-scripts@3.3.0 -g --silent

# COPY . ./

# RUN npm run build

#-------------------

#Nginx

#FROM nginx:alpine

# WORKDIR /usr/share/nginx/html

# RUN rm -rf ./*

# COPY ./build .

# ENTRYPOINT ["nginx", "-g", "daemon off;"]